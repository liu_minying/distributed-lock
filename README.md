# distributed-lock

#### 介绍
基于springboot aop实现的分布式锁工具，实现了基于redisson，mysql，zookeeper的三种分布式锁实现方案。只需一个注解，便能实现对方法的分布式锁。

#### 软件架构
基于springboot start和springboot aop实现。

#### 使用说明

1. 引入maven包
2. 配置分布式锁模式
```properties
#redisson模式。需要自行配置 RedissonClient bean
#echo.lock.config.mode = REDISSON

#数据库模式，基于mysql。需要自行配置 DataSource bean
#echo.lock.config.mode = DB

#zokeeper模式。需要自行配置 CuratorFramework bean
echo.lock.config.mode = ZOOKEEPER
```
3.给需要加锁的方法加上@DistributedLock注解
```java
    /**
    * 分布式锁的key值通过@DistributedLock注解的key字段进行配置
    * 通过#号分割多个参数
    * 通过xxx.xx指定对象中的某个属性或者map中的某个key
    * 通过xxx[n]在指定数组中的第n个值
    */
    @DistributedLock(key = "a")
    public void lockTest(String a) {
        log.info("开始执行。。。。。。");
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("执行结束。。。。。。");
    }
```

#### 特性

1.  只需一个注解快速实现分布式锁
2.  兼容市面上主流的三种分布式锁实现方案
