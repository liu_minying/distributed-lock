package com.echo.distributedlock.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.concurrent.TimeUnit;

/**
 * @author lmy
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface IdempotentLock {

    /**
     * 幂等性校验key
     * 方法的参数名称,如果是对象里面的字段用“.”隔开
     */
    String key();

    /**
     * 时间类型
     */
    TimeUnit timeUnit();

    /**
     * 时间长度
     */
    int longTime();
}
