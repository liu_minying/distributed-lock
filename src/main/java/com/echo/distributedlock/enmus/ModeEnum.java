package com.echo.distributedlock.enmus;

import java.util.Objects;

/**
 * @author lmy
 */
public enum ModeEnum {

    /**
     * 基于数据库的分布式锁
     */
    DB,
    /**
     * 基于redisson的分布式锁
     */
    REDISSON,
    /**
     * 基于zookeeper的分布式锁
     */
    ZOOKEEPER;

    public static ModeEnum chooseMode(String mode) {
        for (ModeEnum modeEnum : ModeEnum.values()) {
            if (Objects.equals(mode, modeEnum.name())) {
                return modeEnum;
            }
        }
        throw new IllegalArgumentException("分布式锁执行模式不存在");
    }
}
