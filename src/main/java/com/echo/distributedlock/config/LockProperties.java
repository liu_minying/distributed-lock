package com.echo.distributedlock.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author lmy
 */
@Component
@ConfigurationProperties(prefix = "echo.lock.config")
public class LockProperties {

    private String mode;

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }
}
