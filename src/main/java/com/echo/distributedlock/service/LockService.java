package com.echo.distributedlock.service;

import java.util.concurrent.locks.Lock;

/**
 * @author lmy
 */
public interface LockService {

    /**
     * 获取分布式锁
     * @param key 分布式锁key值
     * @return 分布式锁
     */
    Lock getLock(String key);
}
