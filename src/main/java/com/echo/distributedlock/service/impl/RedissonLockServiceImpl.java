package com.echo.distributedlock.service.impl;

import com.echo.distributedlock.service.LockService;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.concurrent.locks.Lock;

/**
 * @author lmy
 */
@Service
public class RedissonLockServiceImpl implements LockService {

    @Resource
    private RedissonClient redissonClient;

    @Override
    public Lock getLock(String key) {
        return redissonClient.getLock(key);
    }
}
