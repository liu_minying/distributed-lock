package com.echo.distributedlock.service.impl;

import com.echo.distributedlock.api.ZooLock;
import com.echo.distributedlock.service.LockService;
import org.apache.curator.framework.CuratorFramework;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.concurrent.locks.Lock;

/**
 * @author lmy
 */
@Service
public class ZooKeeperLockServiceImpl implements LockService {

    @Resource
    private CuratorFramework curatorFramework;

    @Override
    public Lock getLock(String key) {
        return new ZooLock("/" + key, curatorFramework);
    }
}
